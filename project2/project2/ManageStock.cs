﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace project2
{
    public partial class ManageStock : Form
    {
        public ManageStock()
        {
            InitializeComponent();
        }

		private void ManageStock_Load(object sender, EventArgs e)
		{

		}

		private void btnViewFormAddStock_Click(object sender, EventArgs e)
		{
			FormManager.ShowForm("addStock");
			Hide();
		}

		private void btnViewFormManageSales_Click(object sender, EventArgs e)
		{
			FormManager.ShowForm("viewManageSales");
			Hide();
		}

		private void btnViewFormManageStock_Click(object sender, EventArgs e)
		{
			FormManager.ShowForm("manageStock");
			Hide();
		}

		private void btnViewFormAddSale_Click(object sender, EventArgs e)
		{
			FormManager.ShowForm("addSale");
			Hide();
		}
	}
}
