﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace project2
{
    public class OrderRecord
    {
		List<SaleRecord> sales;
		int transactionID;

		public OrderRecord(int transID)
		{
			sales = new List<SaleRecord>();
			transactionID = transID;
		}

		public int TransactionID { get => transactionID; }
		public List<SaleRecord> Sales { get => sales; set => sales = value; }

		public void AddSale(SaleRecord toAdd)
		{
			sales.Add(toAdd);
		}
	}
}
