﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2
{
    class StockRecord
    {
        int id;
        string name;

        StockRecord(int aID, string aName)
        {
            id = aID;
            name = aName;
        }

        public int ID { get => id; }
        public string Name { get => name; }
    }
}
