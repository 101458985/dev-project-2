﻿using System;

namespace project2
{
    partial class AddStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnSaveAndQuit = new System.Windows.Forms.Button();
            this.btnViewFormSettings = new System.Windows.Forms.Button();
            this.btnViewFormAddStock = new System.Windows.Forms.Button();
            this.btnViewFormManageSales = new System.Windows.Forms.Button();
            this.btnViewFormAddSale = new System.Windows.Forms.Button();
            this.comboBoxStock = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownPrice = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownStock = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonDiscard = new System.Windows.Forms.Button();
            this.buttonNew = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCharts = new System.Windows.Forms.Button();
            this.pnlMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStock)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnlMenu.Controls.Add(this.btnCharts);
            this.pnlMenu.Controls.Add(this.btnSaveAndQuit);
            this.pnlMenu.Controls.Add(this.btnViewFormSettings);
            this.pnlMenu.Controls.Add(this.btnViewFormAddStock);
            this.pnlMenu.Controls.Add(this.btnViewFormManageSales);
            this.pnlMenu.Controls.Add(this.btnViewFormAddSale);
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(939, 58);
            this.pnlMenu.TabIndex = 1;
            // 
            // btnSaveAndQuit
            // 
            this.btnSaveAndQuit.Location = new System.Drawing.Point(760, 14);
            this.btnSaveAndQuit.Name = "btnSaveAndQuit";
            this.btnSaveAndQuit.Size = new System.Drawing.Size(163, 28);
            this.btnSaveAndQuit.TabIndex = 1;
            this.btnSaveAndQuit.Text = "Save & Quit";
            this.btnSaveAndQuit.UseVisualStyleBackColor = true;
            // 
            // btnViewFormSettings
            // 
            this.btnViewFormSettings.Location = new System.Drawing.Point(677, 14);
            this.btnViewFormSettings.Name = "btnViewFormSettings";
            this.btnViewFormSettings.Size = new System.Drawing.Size(77, 28);
            this.btnViewFormSettings.TabIndex = 1;
            this.btnViewFormSettings.Text = "Settings";
            this.btnViewFormSettings.UseVisualStyleBackColor = true;
            // 
            // btnViewFormAddStock
            // 
            this.btnViewFormAddStock.Location = new System.Drawing.Point(277, 14);
            this.btnViewFormAddStock.Name = "btnViewFormAddStock";
            this.btnViewFormAddStock.Size = new System.Drawing.Size(109, 28);
            this.btnViewFormAddStock.TabIndex = 1;
            this.btnViewFormAddStock.Text = "Add Stock";
            this.btnViewFormAddStock.UseVisualStyleBackColor = true;
            this.btnViewFormAddStock.Click += new System.EventHandler(this.btnViewFormAddStock_Click);
            // 
            // btnViewFormManageSales
            // 
            this.btnViewFormManageSales.Location = new System.Drawing.Point(130, 14);
            this.btnViewFormManageSales.Name = "btnViewFormManageSales";
            this.btnViewFormManageSales.Size = new System.Drawing.Size(141, 28);
            this.btnViewFormManageSales.TabIndex = 1;
            this.btnViewFormManageSales.Text = "View / Manage Sales";
            this.btnViewFormManageSales.UseVisualStyleBackColor = true;
            this.btnViewFormManageSales.Click += new System.EventHandler(this.btnViewFormManageSales_Click);
            // 
            // btnViewFormAddSale
            // 
            this.btnViewFormAddSale.Location = new System.Drawing.Point(15, 14);
            this.btnViewFormAddSale.Name = "btnViewFormAddSale";
            this.btnViewFormAddSale.Size = new System.Drawing.Size(109, 28);
            this.btnViewFormAddSale.TabIndex = 0;
            this.btnViewFormAddSale.Text = "Add Sale";
            this.btnViewFormAddSale.UseVisualStyleBackColor = true;
            this.btnViewFormAddSale.Click += new System.EventHandler(this.btnViewFormAddSale_Click);
            // 
            // comboBoxStock
            // 
            this.comboBoxStock.FormattingEnabled = true;
            this.comboBoxStock.Location = new System.Drawing.Point(286, 187);
            this.comboBoxStock.Name = "comboBoxStock";
            this.comboBoxStock.Size = new System.Drawing.Size(331, 21);
            this.comboBoxStock.TabIndex = 2;
            this.comboBoxStock.SelectedIndexChanged += new System.EventHandler(this.comboBoxStock_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(297, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Stock Management";
            // 
            // numericUpDownPrice
            // 
            this.numericUpDownPrice.DecimalPlaces = 2;
            this.numericUpDownPrice.Location = new System.Drawing.Point(147, 120);
            this.numericUpDownPrice.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDownPrice.Name = "numericUpDownPrice";
            this.numericUpDownPrice.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownPrice.TabIndex = 5;
            // 
            // numericUpDownStock
            // 
            this.numericUpDownStock.Location = new System.Drawing.Point(147, 161);
            this.numericUpDownStock.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDownStock.Name = "numericUpDownStock";
            this.numericUpDownStock.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownStock.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(71, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Item ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Item Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Item Price";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Item Stock";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(147, 71);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(188, 20);
            this.textBoxName.TabIndex = 11;
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(660, 185);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(75, 23);
            this.buttonLoad.TabIndex = 12;
            this.buttonLoad.Text = "Load Item";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(147, 23);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.ReadOnly = true;
            this.textBoxID.Size = new System.Drawing.Size(100, 20);
            this.textBoxID.TabIndex = 13;
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(376, 566);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(185, 57);
            this.buttonUpdate.TabIndex = 14;
            this.buttonUpdate.Text = "Update Stock";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click_1);
            // 
            // buttonDiscard
            // 
            this.buttonDiscard.Location = new System.Drawing.Point(598, 600);
            this.buttonDiscard.Name = "buttonDiscard";
            this.buttonDiscard.Size = new System.Drawing.Size(137, 23);
            this.buttonDiscard.TabIndex = 15;
            this.buttonDiscard.Text = "Discard Changes";
            this.buttonDiscard.UseVisualStyleBackColor = true;
            // 
            // buttonNew
            // 
            this.buttonNew.Location = new System.Drawing.Point(760, 185);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(75, 23);
            this.buttonNew.TabIndex = 16;
            this.buttonNew.Text = "New Item";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxID);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxName);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.numericUpDownPrice);
            this.panel1.Controls.Add(this.numericUpDownStock);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(277, 272);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(433, 192);
            this.panel1.TabIndex = 17;
            // 
            // btnCharts
            // 
            this.btnCharts.Location = new System.Drawing.Point(392, 14);
            this.btnCharts.Name = "btnCharts";
            this.btnCharts.Size = new System.Drawing.Size(109, 28);
            this.btnCharts.TabIndex = 3;
            this.btnCharts.Text = "Charts";
            this.btnCharts.UseVisualStyleBackColor = true;
            this.btnCharts.Click += new System.EventHandler(this.btnCharts_Click);
            // 
            // AddStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 787);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonNew);
            this.Controls.Add(this.buttonDiscard);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxStock);
            this.Controls.Add(this.pnlMenu);
            this.Name = "AddStock";
            this.Text = "Form3";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.pnlMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStock)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void btnViewFormManageStock_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnSaveAndQuit;
        private System.Windows.Forms.Button btnViewFormSettings;
        private System.Windows.Forms.Button btnViewFormAddStock;
        private System.Windows.Forms.Button btnViewFormManageSales;
        private System.Windows.Forms.Button btnViewFormAddSale;
        private System.Windows.Forms.ComboBox comboBoxStock;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownPrice;
        private System.Windows.Forms.NumericUpDown numericUpDownStock;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonDiscard;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCharts;
    }
}