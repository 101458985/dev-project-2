﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace project2
{

    public partial class AddStock : Form
    {

        int highestID;

        public void loadStock (Stock aStock)
        {
            textBoxID.Text = aStock.stockID.ToString();
            numericUpDownPrice.Value = (decimal) aStock.price;
            numericUpDownStock.Value = (decimal)aStock.qty;
            textBoxName.Text = aStock.name.ToString();
        }

        public string printStock (Stock aStock)
        {
            string output = "";
            output = aStock.stockID.ToString() + "," + aStock.name + "," + aStock.qty.ToString() + "," + aStock.price.ToString();
            return output;
        }

        public int GetHighestID(List<Stock> aList)
        {
            int result = 0;
            foreach (Stock s in aList)
            {
                if (s.stockID > result) result = s.stockID;
            }
            return result;
        }

        public void saveStock (List<Stock> aList)
        {
            string tempString;
           
            System.IO.StreamWriter fileWrite = new System.IO.StreamWriter("stock.txt");
            //System.IO.File.Create("stock.txt").Dispose();  -- this would close the streamwriter before it writes
            foreach (Stock aStock in aList)
            {
                tempString = printStock(aStock);
                fileWrite.WriteLine(tempString);
            }
            fileWrite.Close();
            // return true;
        }

        public List<Stock> stockList = new List<Stock>();

        public AddStock()
        {
            InitializeComponent();
            // check to ensure file is created
            if (!File.Exists("stock.txt"))
            {
                System.IO.File.Create("stock.txt").Dispose();
            }

            string aLine;
            string[] words;
            int tempQty;
            int tempID;
            double tempPrice;
            string tempName;


            System.IO.StreamReader file = new System.IO.StreamReader("stock.txt");
            while ((aLine = file.ReadLine()) != null)
            {
                words = aLine.Split(',');
                int.TryParse(words[0], out tempID);
                tempName = words[1];
                int.TryParse(words[2], out tempQty);
                double.TryParse(words[3], out tempPrice);
                Stock aStock = new Stock(tempName, tempID, tempQty, tempPrice);
                stockList.Add(aStock);

                comboBoxStock.Items.Add(tempName);

            }
            file.Close();

            highestID = GetHighestID(stockList) + 1;
            textBoxID.Text = highestID.ToString();

            // pass stock list through to rest of program. 
            FormManager.Stock = stockList;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {

        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            int indexOfChoice = -1;
            // check that combo box is not empty - otherwise program will crash if nothing is selected.
            if (comboBoxStock.SelectedItem != null)
            {
                string currentChoice = comboBoxStock.SelectedItem.ToString();
                int maxChoice = comboBoxStock.Items.Count;
                for (int i = 0; i < maxChoice; i++)
                {
                    if (stockList.ElementAt(i).name == currentChoice)
                    {
                        loadStock(stockList.ElementAt(i));
                    }
                }
            }
        }

        private void btnSaveAndQuit_Click(object sender, EventArgs e)
        {
            FormManager.WriteFile();
        }

        private void buttonUpdate_Click_1(object sender, EventArgs e)
        {
            // must initialise a stock item to add to list or it won't be saved to file
            int id = Convert.ToInt32(textBoxID.Text);
            String name = textBoxName.Text;
            double price = Convert.ToDouble(numericUpDownPrice.Value);
            int quantity = Convert.ToInt32(numericUpDownStock.Value);

            Stock aStock = new Stock(name, id, quantity, price);
            // add stock to list
            stockList.Add(aStock);
            // update button should trigger the save function and add stock item to list .
            saveStock(stockList);
            // should add stock item to the combo box.
            comboBoxStock.Items.Add(aStock.name);

            highestID++; // initialise the next ID number.


        }

        private void comboBoxStock_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach(Stock item in stockList)
            {
                if (item.name == (String) comboBoxStock.SelectedItem)
                {
                    loadStock(item);
                }
            }
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            // new button should clear fields and initialise a new ID number.
            textBoxID.Text = highestID.ToString();
            textBoxName.Text = "";
            numericUpDownStock.Value = 0;
            numericUpDownPrice.Value = 0;

        }

        private void btnViewFormAddStock_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("addStock");
            Hide();
        }

        private void btnViewFormManageSales_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("viewManageSales");
            Hide();
        }

        private void btnViewFormAddSale_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("addSale");
            Hide();
        }

        private void btnCharts_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("history");
            Hide();
        }
    }
}
