﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace project2
{
    public partial class History : Form
    {
        int productID = 0;
        List<SaleRecord> itemSales;
        List<Double> itemPredictions = new List<double>();
        ChartArea area = new ChartArea("First");

        public History()
        {
            InitializeComponent();
            updateTitle();
        }


		private void History_Activated(object sender, EventArgs e)
		{
            CalculatePredictions();
        }


		private void label1_Click(object sender, EventArgs e)
		{
			itemSales = FormManager.Sales;

			itemNameLabel.Text = "Item: " + itemSales[0].ProductID;

			CalculatePredictions();

		}

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void CalculatePredictions()
        {
            int monthQuant;
            int prevMonthTotal = 0;
            int totalUnitsSold = 0;
            double[] xData = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };


            for (int i = 1; i < 13; i++)
            {
                monthQuant = 0;
                

                foreach (SaleRecord s in FormManager.Sales)
                {
                    if (s.SaleDate.Month == i && s.ProductID == productID)
                    {
                        monthQuant += s.Quantity;
                       
                    }
                }

                if(monthQuant == 0 && prevMonthTotal > 1)
                {
                    monthQuant = prevMonthTotal / 2;
                }
                else if(monthQuant == 0)
                {
                    monthQuant = 1;
                }

                prevMonthTotal = monthQuant;

                totalUnitsSold += monthQuant;
                itemPredictions.Add(monthQuant);
            }

            lblAverage.Text = (totalUnitsSold / 12).ToString() ;



            double[] yData = itemPredictions.ToArray();

            area.AxisX.Maximum = 12;
            area.AxisX.Minimum = 1;
            predictionChart.ChartAreas.Add(area);
            Series barSeries = new Series();
            barSeries.Name = "Product Sold";
            barSeries.Points.DataBindXY(xData, yData);
            barSeries.ChartType = SeriesChartType.Bar;
            barSeries.ChartArea = "First";
            
            predictionChart.Series.Add(barSeries);



        }

        private void button6_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("addSale");
            Hide();
        }

        private void History_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void History_Shown(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("viewManageSales");
            Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("addStock");
            Hide();
        }

        private void nudProductID_ValueChanged(object sender, EventArgs e)
        {
            productID = Convert.ToInt32(nudProductID.Value);
            updateTitle();
        }

        private void updateTitle()
        {
            Stock aStock = FormManager.GetStockWithID(productID);
            if (aStock == null)
            {
                itemNameLabel.Text = "Invalid Product ID!";
            } else
            {
                itemNameLabel.Text = aStock.name;
            }
        }

        private void btnGenChart_Click(object sender, EventArgs e)
        {
            predictionChart.ChartAreas.Clear();
            itemPredictions.Clear();
            predictionChart.Series.Clear();
            CalculatePredictions();
        }
    }
}
