﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace project2
{

    public partial class ViewManageSales : Form
    {
        int changedOrderTransactionID;
        int changedSaleTransactionID;

        public ViewManageSales()
        {
            InitializeComponent();
        }

        private DataTable GenerateSalesTable()
        {
            DataTable sales = new DataTable("Sales");

            DataColumn c0 = new DataColumn("TransactionID");
            DataColumn c1 = new DataColumn("ProductID");
            DataColumn c2 = new DataColumn("Quantity");
            DataColumn c3 = new DataColumn("Date & Time");
            DataColumn c4 = new DataColumn("Total Amount");
            DataColumn c5 = new DataColumn("Comments");

			sales.Columns.Add(c0);
			sales.Columns.Add(c1);
			sales.Columns.Add(c2);
			sales.Columns.Add(c3);
			sales.Columns.Add(c4);
			sales.Columns.Add(c5);

			return sales;
        }

        private DataTable GenerateOrdersTable()
        {
            DataTable orders = new DataTable("Orders");

            DataColumn c0 = new DataColumn("TransactionID");

            orders.Columns.Add(c0);

            return orders;
        }

        private void PopulateOrderTable()
        {
            dgvOrders.DataSource = null;
            dgvOrders.Refresh();
            DataTable orders = GenerateOrdersTable();
            foreach(OrderRecord o in FormManager.Orders)
            {
                DataRow row = orders.NewRow();
                row["TransactionID"] = o.TransactionID;
                orders.Rows.Add(row);
            }
            dgvOrders.DataSource = orders;
        
        }
        private void PopulateSaleTable(OrderRecord selected)
        {
            dgvSales.DataSource = null;
            dgvSales.Refresh();
            DataTable sales = GenerateSalesTable();

			if (monthlyToggle.Checked)
			{
				foreach (SaleRecord s in FormManager.GetSaleMonth(Convert.ToInt32(MonthSelect.Value)))
				{
					DataRow row = sales.NewRow();
					row["TransactionID"] = s.TransactionID;
					row["ProductID"] = s.ProductID;
					row["Quantity"] = s.Quantity;
					row["Date & Time"] = s.SaleDate;
					row["Total Amount"] = s.TotalAmount;
					row["Comments"] = s.Comments;
					sales.Rows.Add(row);
				}
			}
			else
			{
				foreach (SaleRecord s in selected.Sales)
				{
					DataRow row = sales.NewRow();
					row["TransactionID"] = s.TransactionID;
					row["ProductID"] = s.ProductID;
					row["Quantity"] = s.Quantity;
					row["Date & Time"] = s.SaleDate;
					row["Total Amount"] = s.TotalAmount;
					row["Comments"] = s.Comments;
					sales.Rows.Add(row);
				}
			}
            dgvSales.DataSource = sales;
        }

        private void PopulateSaleTable()
        {
            dgvSales.DataSource = null;
            dgvSales.Refresh();
            DataTable sales = GenerateSalesTable();

            if (monthlyToggle.Checked)
            {
                foreach (SaleRecord s in FormManager.GetSaleMonth(Convert.ToInt32(MonthSelect.Value)))
                {
                    DataRow row = sales.NewRow();
                    row["TransactionID"] = s.TransactionID;
                    row["ProductID"] = s.ProductID;
                    row["Quantity"] = s.Quantity;
                    row["Date & Time"] = s.SaleDate;
                    row["Total Amount"] = s.TotalAmount;
                    row["Comments"] = s.Comments;
                    sales.Rows.Add(row);
                }
            }
            dgvSales.DataSource = sales;
        }

        private void btnViewFormAddStock_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("addStock");
            Hide();
        }

        private void btnViewFormAddSale_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("addSale");
            Hide();
        }

        private void btnCharts_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("history");
            Hide();
        }

        private void btnViewFormManageSales_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("viewManageSales");
            Hide();
        }

        private void dgvSales_SelectionChanged(object sender, EventArgs e)
        {
            // get selected row
            if (dgvSales.SelectedRows.Count > 0)
            {
                DataGridViewRow selected = dgvSales.SelectedRows[0];
                changedSaleTransactionID = Convert.ToInt32(selected.Cells[0].Value) + Convert.ToInt32(selected.Cells[1].Value);
                numQuantity.Value = Convert.ToInt32(selected.Cells[2].Value);
                dtpDate.Value = Convert.ToDateTime(selected.Cells[3].Value);
                numTotalAmount.Value = Convert.ToDecimal(selected.Cells[4].Value);
                txtComment.Text = selected.Cells[5].Value.ToString();
            }
        }

        private void btnSaveChanges_Click(object sender, EventArgs e)
        {
            SaveChanges();
            PopulateOrderTable();
        }

        private void SaveChanges()
        {
            foreach(OrderRecord o in FormManager.Orders)
            {
                if (o.TransactionID == changedOrderTransactionID)
                {
                    foreach(SaleRecord s in o.Sales)
                    {
                        if (s.Key == changedSaleTransactionID)
                        {
                            s.Quantity = Convert.ToInt32(numQuantity.Value);
                            s.SaleDate = dtpDate.Value;
                            s.TotalAmount = numTotalAmount.Value;
                            s.Comments = txtComment.Text;
                        }
                    }
                }
            }
			
			FormManager.WriteFile();
		}

        private void ViewManageSales_Activated(object sender, EventArgs e)
        {
            if (FormManager.Sales.Count == 0)
            {
                FormManager.ReadSaleFile();
            }
            PopulateOrderTable();
        }

        private void btnSaveAndQuit_Click(object sender, EventArgs e)
        {
            FormManager.WriteFile();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgvOrders_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvOrders.SelectedRows.Count > 0)
            {
                DataGridViewRow selected = dgvOrders.SelectedRows[0];
                int OrderID = Convert.ToInt32(selected.Cells[0].Value);
                foreach(OrderRecord o in FormManager.Orders)
                {
                    if(o.TransactionID == OrderID)
                    {
                        changedOrderTransactionID = Convert.ToInt32(selected.Cells[0].Value);
                        PopulateSaleTable(o);
                    }
                }


            }
        }

		private void numericUpDown1_ValueChanged(object sender, EventArgs e)
		{
            if (monthlyToggle.Checked)
            {
                dgvOrders.Visible = false;
                pnlEdit.Visible = false;
                PopulateSaleTable();
            }
            else
            {
                dgvOrders.Visible = true;
                pnlEdit.Visible = true;
                PopulateSaleTable();
            }
        }

		private void txtComment_TextChanged(object sender, EventArgs e)
		{

		}

        private void monthlyToggle_CheckedChanged_1(object sender, EventArgs e)
        {
            if(monthlyToggle.Checked)
            {
                dgvOrders.Visible = false;
                pnlEdit.Visible = false;
                btnGenerateReport.Visible = true;
                PopulateSaleTable();
            } else
            {
                dgvOrders.Visible = true;
                pnlEdit.Visible = true;
                btnGenerateReport.Visible = false;
                PopulateSaleTable();
            }
           
        }

        private void btnGenerateReport_Click(object sender, EventArgs e)
        {
            List<SaleRecord> monthlySales = FormManager.GetSaleMonth(Convert.ToInt32(MonthSelect.Value));
            Dictionary<string, int> stockSales = new Dictionary<string, int>();
            string productName = "";
            foreach (Stock s in FormManager.Stock)
            {
                stockSales.Add(s.name, 0);
            }

            foreach(SaleRecord s in monthlySales)
            {
                Stock product = FormManager.GetStockWithID(s.ProductID);
                if (product != null)
                {
                    productName = product.name;
                }
                if (stockSales.ContainsKey(productName))
                {
                    stockSales[productName] = stockSales[productName] + s.Quantity;
                }
            }
            FormManager.WriteMonthlyReportCSV(stockSales, Convert.ToInt32(MonthSelect.Value));
            MessageBox.Show("Created Report CSV file");
        }
    }
}
