﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace project2
{
    public partial class AddSale : Form
    {
        OrderRecord currentOrder;
        int transID;
        decimal prevValue;

        public AddSale()
        {
            InitializeComponent();
            lblSelectedItem.Text = "";
            setSelectedItemLabel();

            FormManager.ReadSaleFile();

            currentOrder = FormManager.GetNewOrder();

        }


        private void btnSaveAndQuit_Click(object sender, EventArgs e)
        {
            FormManager.WriteFile();
        }

        private void AddSale_Load(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void totalAmount_Click(object sender, EventArgs e)
        {

        }

        private void productID_label_Click(object sender, EventArgs e)
        {

        }

        private void btnViewFormAddStock_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("addStock");
            Hide();
        }

        private void btnViewFormManageSales_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("viewManageSales");
            Hide();
        }

        private void btnViewFormAddSale_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("addSale");
            Hide();
        }

        private void btnViewCharts_Click(object sender, EventArgs e)
        {
            FormManager.ShowForm("history");
            Hide();
        }

        private void confirmButton_Click(object sender, EventArgs e)
        {
            string errMsg = "";
            string warnMsg = "";
            foreach (SaleRecord aSale in currentOrder.Sales)
            {
                foreach (Stock aStock in FormManager.Stock)
                {
                    if (aStock.stockID == aSale.ProductID)
                    {
                        if (aSale.Quantity > aStock.qty)
                        {
                            errMsg += "Insufficient stock to complete order for: " + aSale.ProductID.ToString() + '\n';
                        }
                        else if (aStock.qty - aSale.Quantity < 5)
                        {
                            warnMsg += "Stock getting low for: " + aStock.stockID.ToString() + '\n';
                        }
                    }
                }
            }
            if (errMsg == "")
            {
                if (warnMsg != "")
                {
                    MessageBox.Show(errMsg);
                }
                FormManager.Orders.Add(currentOrder);
                currentOrder = FormManager.GetNewOrder();
                FormManager.WriteFile();
            }
            else
            {
                MessageBox.Show(errMsg);
            }
        }

        private void addItemButton_Click(object sender, EventArgs e)
        {
            SaleRecord toAdd = new SaleRecord();
            toAdd.ProductID = Convert.ToInt32(productID.Value);
            toAdd.Quantity = Convert.ToInt32(quantity.Value);

            toAdd.TotalAmount = totalAmount.Value;
            toAdd.SaleDate = dateTime.Value;
            toAdd.Comments = comments.Text;

            toAdd.TransactionID = currentOrder.TransactionID;

            productID.Value = 0;
            quantity.Value = 0;
            totalAmount.Value = 0;
            dateTime.ResetText();
            comments.ResetText();

            currentOrder.AddSale(toAdd);
            FormManager.Sales.Add(toAdd);
        }

        private void productID_ValueChanged(object sender, EventArgs e)
        {
            if (prevValue != productID.Value)
            {
                setSelectedItemLabel();
            }
            prevValue = productID.Value;
        }

        private void setSelectedItemLabel()
        {
            Stock aStock = FormManager.GetStockWithID(Convert.ToInt32(productID.Value));
            if (aStock == null)
            {
                lblSelectedItem.Text = "Warning: No stock item exists with this ID!";
                lblSelectedItem.ForeColor = Color.Red;
            }
            else
            {
                lblSelectedItem.Text = aStock.name;
                lblSelectedItem.ForeColor = Color.Green;
            }
            updateAmount(aStock);
        }

        private void quantity_ValueChanged(object sender, EventArgs e)
        {
            Stock aStock = FormManager.GetStockWithID(Convert.ToInt32(productID.Value));
            updateAmount(aStock);
        }

        private void updateAmount(Stock aStock)
        {
            if (aStock != null)
            {
                double cost = aStock.price;
                totalAmount.Value = (quantity.Value * Convert.ToDecimal(cost));
            }
        }
    }
}
