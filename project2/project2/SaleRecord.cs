﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2
{
	public class SaleRecord
	{
		int transactionID;
		int productID;
		int quantity;
		DateTime saleDate;
		decimal totalAmount;
		string comments;

		public SaleRecord()
		{
			transactionID = 0;
			productID = 0;
			quantity = 0;
			saleDate = new DateTime();
			totalAmount = 0;
			comments = "";
		}

		public int TransactionID { get => transactionID; set => transactionID = value; }
		public int ProductID { get => productID; set => productID = value; }
		public int Quantity { get => quantity; set => quantity = value; }
		public DateTime SaleDate { get => saleDate; set => saleDate = value; }
		public decimal TotalAmount { get => totalAmount; set => totalAmount = value; }
		public string Comments { get => comments; set => comments = value; }

		public int Key
		{
			get
			{
				return transactionID + productID;
			}
		}

		public string GetCSV()
		{
			string returnString = "";

			returnString += TransactionID + ",";
			returnString += ProductID + ",";
			returnString += Quantity + ",";
			returnString += SaleDate.ToString() + ",";
			returnString += TotalAmount.ToString() + ",";
			returnString += Comments;

			return returnString;
		}
	}
}
