﻿namespace project2
{
    partial class History
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.predictionChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.itemNameLabel = new System.Windows.Forms.Label();
            this.nudProductID = new System.Windows.Forms.NumericUpDown();
            this.lblProductID = new System.Windows.Forms.Label();
            this.btnGenChart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAverage = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.predictionChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudProductID)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(939, 58);
            this.panel1.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(760, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 28);
            this.button1.TabIndex = 1;
            this.button1.Text = "Save & Quit";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(677, 14);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(77, 28);
            this.button3.TabIndex = 1;
            this.button3.Text = "Settings";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(277, 14);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(109, 28);
            this.button4.TabIndex = 1;
            this.button4.Text = "Add Stock";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(130, 14);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(141, 28);
            this.button5.TabIndex = 1;
            this.button5.Text = "View / Manage Sales";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(15, 14);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(109, 28);
            this.button6.TabIndex = 0;
            this.button6.Text = "Add Sale";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // predictionChart
            // 
            legend1.Name = "Legend1";
            this.predictionChart.Legends.Add(legend1);
            this.predictionChart.Location = new System.Drawing.Point(15, 114);
            this.predictionChart.Name = "predictionChart";
            this.predictionChart.Size = new System.Drawing.Size(908, 399);
            this.predictionChart.TabIndex = 7;
            this.predictionChart.Text = "predictionChart";
            title1.Alignment = System.Drawing.ContentAlignment.BottomCenter;
            title1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title1.Name = "Title1";
            title1.Text = "Sales";
            title2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Left;
            title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title2.Name = "Title2";
            title2.Text = "Month";
            this.predictionChart.Titles.Add(title1);
            this.predictionChart.Titles.Add(title2);
            // 
            // itemNameLabel
            // 
            this.itemNameLabel.AutoSize = true;
            this.itemNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemNameLabel.Location = new System.Drawing.Point(436, 71);
            this.itemNameLabel.Name = "itemNameLabel";
            this.itemNameLabel.Size = new System.Drawing.Size(102, 29);
            this.itemNameLabel.TabIndex = 8;
            this.itemNameLabel.Text = "[NAME]";
            this.itemNameLabel.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // nudProductID
            // 
            this.nudProductID.Location = new System.Drawing.Point(803, 80);
            this.nudProductID.Name = "nudProductID";
            this.nudProductID.Size = new System.Drawing.Size(120, 20);
            this.nudProductID.TabIndex = 9;
            this.nudProductID.ValueChanged += new System.EventHandler(this.nudProductID_ValueChanged);
            // 
            // lblProductID
            // 
            this.lblProductID.AutoSize = true;
            this.lblProductID.Location = new System.Drawing.Point(830, 64);
            this.lblProductID.Name = "lblProductID";
            this.lblProductID.Size = new System.Drawing.Size(55, 13);
            this.lblProductID.TabIndex = 10;
            this.lblProductID.Text = "ProductID";
            // 
            // btnGenChart
            // 
            this.btnGenChart.Location = new System.Drawing.Point(15, 65);
            this.btnGenChart.Name = "btnGenChart";
            this.btnGenChart.Size = new System.Drawing.Size(109, 23);
            this.btnGenChart.TabIndex = 11;
            this.btnGenChart.Text = "Generate Chart";
            this.btnGenChart.UseVisualStyleBackColor = true;
            this.btnGenChart.Click += new System.EventHandler(this.btnGenChart_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 520);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(314, 25);
            this.label1.TabIndex = 12;
            this.label1.Text = "Average Units Sold / Month :";
            // 
            // lblAverage
            // 
            this.lblAverage.AutoSize = true;
            this.lblAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblAverage.Location = new System.Drawing.Point(333, 520);
            this.lblAverage.Name = "lblAverage";
            this.lblAverage.Size = new System.Drawing.Size(25, 25);
            this.lblAverage.TabIndex = 13;
            this.lblAverage.Text = "0";
            // 
            // History
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(939, 582);
            this.Controls.Add(this.lblAverage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGenChart);
            this.Controls.Add(this.lblProductID);
            this.Controls.Add(this.nudProductID);
            this.Controls.Add(this.itemNameLabel);
            this.Controls.Add(this.predictionChart);
            this.Controls.Add(this.panel1);
            this.Name = "History";
            this.Text = "History";
            this.Load += new System.EventHandler(this.History_Load);
            this.Shown += new System.EventHandler(this.History_Shown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.predictionChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudProductID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataVisualization.Charting.Chart predictionChart;
        private System.Windows.Forms.Label itemNameLabel;
        private System.Windows.Forms.NumericUpDown nudProductID;
        private System.Windows.Forms.Label lblProductID;
        private System.Windows.Forms.Button btnGenChart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAverage;
    }
}