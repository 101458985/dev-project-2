﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace project2
{
	public static class FormManager
	{
		const string SALES_FILENAME = "sales_data.txt";
        const string MONTHLY_REPORT_FORMAT = "report.txt";

		static List<SaleRecord> sales = new List<SaleRecord>();
		static List<OrderRecord> orders = new List<OrderRecord>();
        static List<Stock> stock = new List<Stock>();

        static Form addStock = new AddStock();
        static Form addSale = new AddSale();
		static Form viewManageSales = new ViewManageSales();
		static Form manageStock = new ManageStock();
		static Form history = new History();

		internal static List<SaleRecord> Sales { get => sales; set => sales = value; }
        internal static List<OrderRecord> Orders { get => orders; set => orders = value; }
        internal static List<Stock> Stock { get => stock; set => stock = value; }


		public static void WriteFile()
		{
			StreamWriter writer = new StreamWriter(SALES_FILENAME);

			string[] toWrite = new string[sales.Count];

			for( int i = 0; i < sales.Count; i++)
			{
				toWrite[i] = sales[i].GetCSV();
			}

			foreach (string s in toWrite)
			{
				writer.WriteLine(s);
			}

			writer.WriteLine("");

            writer.Close();
		}

		public static void ShowForm(string formName)
		{
			switch(formName)
			{
				case "addSale":
					addSale.Show();
					break;
				case "viewManageSales":
					viewManageSales.Show();
					break;
				case "addStock":
					addStock.Show();
					break;
				case "manageStock":
					manageStock.Show();
					break;
				case "history":
                    History newHistory = new History();
					newHistory.Show();
					break;
			}
		}

        public static void ReadSaleFile()
        {
            Sales.Clear();
            Orders.Clear();
            string line;
            StreamReader file = new StreamReader(SALES_FILENAME);

            while ((line = file.ReadLine()) != "" && line != null)
            {
                SaleRecord toDisplay = ConvertStringToSale(line);
                if (toDisplay != null)
                    Sales.Add(ConvertStringToSale(line));
            }

            file.Close();

			List<int> TransactionIDs = new List<int>();

			foreach(SaleRecord _s in Sales)
			{
				if( TransactionIDs.Contains(_s.TransactionID))
				{
					foreach(OrderRecord _o in Orders)
					{
						if (_o.TransactionID == _s.TransactionID)
						{
							_o.AddSale(_s);
						} 
					}
				}
				else
				{
					OrderRecord new_order = new OrderRecord(_s.TransactionID);
					new_order.AddSale(_s);

					Orders.Add(new_order);

                    TransactionIDs.Add(_s.TransactionID);
                }
            }
        }

        private static SaleRecord ConvertStringToSale(string saleString)
        {
            SaleRecord result = new SaleRecord();

            if (saleString != null)
            {
                string[] saleData = saleString.Split(',');

                // default values
                int i = 0;
                decimal f = 0;
                DateTime d;

                if (Int32.TryParse(saleData[0], out i))
                {
                    result.TransactionID = i;
                }
                else
                {
                    result.TransactionID = 0;
                }

                if (Int32.TryParse(saleData[1], out i))
                {
                    result.ProductID = i;
                }
                else
                {
                    result.ProductID = 0;
                }

                if (Int32.TryParse(saleData[2], out i))
                {
                    result.Quantity = i;
                }
                else
                {
                    result.Quantity = 0;
                }

                if (DateTime.TryParse(saleData[3], out d))
                {
                    result.SaleDate = d;
                }
                else
                {
                    result.SaleDate = new DateTime();
                }

                if (decimal.TryParse(saleData[4], out f))
                {
                    result.TotalAmount = f;
                }
                else
                {
                    result.TotalAmount = 0;
                }

                if (saleData[5] != null)
                {
                    result.Comments = saleData[5];
                }
                else
                {
                    result.Comments = "";
                }

                return result;
            }

            return null;
		}

		public static List<OrderRecord> GetOrderMonth(int month)
		{
			List<OrderRecord> toReturn = new List<OrderRecord>();

			foreach (OrderRecord o in Orders)
			{
				if (o.Sales[0].SaleDate.Month == month)
				{
					toReturn.Add(o);
				}
			}

			return toReturn;
		}

		public static List<SaleRecord> GetSaleMonth(int month)
		{
			List<SaleRecord> toReturn = new List<SaleRecord>();

			foreach (SaleRecord s in Sales)
			{
				if (s.SaleDate.Month == month)
				{
					toReturn.Add(s);
				}
			}

			return toReturn;
		}

        public static Stock GetStockWithID(int aId)
        {
            Stock result = null;
            foreach(Stock s in stock)
            {
                if (s.stockID == aId)
                {
                    result = s;
                }
            }
            return result;
        }

        public static OrderRecord GetNewOrder()
        {
            int id = 0;
            int high = 0; 
            foreach(OrderRecord o in orders)
            {
                if (o.TransactionID >= id)
                {
                    high = o.TransactionID;
                }
            }
            high++;
            return new OrderRecord(high);
        }

        public static void WriteMonthlyReportCSV(Dictionary<String, int> stockItems, int month)
        {
            string filename = month.ToString() + MONTHLY_REPORT_FORMAT;
            StreamWriter writer = new StreamWriter(filename);
            foreach(KeyValuePair<string, int> entry in stockItems)
            {
                writer.WriteLine(entry.Key + "," + entry.Value);
            }


            writer.Close();
        }
	}
}
