﻿namespace project2
{
    partial class AddSale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnSaveAndQuit = new System.Windows.Forms.Button();
            this.btnViewCharts = new System.Windows.Forms.Button();
            this.btnViewFormSettings = new System.Windows.Forms.Button();
            this.btnViewFormAddStock = new System.Windows.Forms.Button();
            this.btnViewFormManageSales = new System.Windows.Forms.Button();
            this.btnViewFormAddSale = new System.Windows.Forms.Button();
            this.Title = new System.Windows.Forms.Label();
            this.productIdLabel = new System.Windows.Forms.Label();
            this.totalAmountLabel = new System.Windows.Forms.Label();
            this.quantityLabel = new System.Windows.Forms.Label();
            this.quantity = new System.Windows.Forms.NumericUpDown();
            this.comments = new System.Windows.Forms.TextBox();
            this.commentsLabel = new System.Windows.Forms.Label();
            this.dateTime = new System.Windows.Forms.DateTimePicker();
            this.dateTimeLabel = new System.Windows.Forms.Label();
            this.confirmButton = new System.Windows.Forms.Button();
            this.addItemButton = new System.Windows.Forms.Button();
            this.productID = new System.Windows.Forms.NumericUpDown();
            this.totalAmount = new System.Windows.Forms.NumericUpDown();
            this.lblSelectedItem = new System.Windows.Forms.Label();
            this.pnlMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnlMenu.Controls.Add(this.btnSaveAndQuit);
            this.pnlMenu.Controls.Add(this.btnViewCharts);
            this.pnlMenu.Controls.Add(this.btnViewFormSettings);
            this.pnlMenu.Controls.Add(this.btnViewFormAddStock);
            this.pnlMenu.Controls.Add(this.btnViewFormManageSales);
            this.pnlMenu.Controls.Add(this.btnViewFormAddSale);
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(939, 58);
            this.pnlMenu.TabIndex = 1;
            // 
            // btnSaveAndQuit
            // 
            this.btnSaveAndQuit.Location = new System.Drawing.Point(760, 14);
            this.btnSaveAndQuit.Name = "btnSaveAndQuit";
            this.btnSaveAndQuit.Size = new System.Drawing.Size(163, 28);
            this.btnSaveAndQuit.TabIndex = 1;
            this.btnSaveAndQuit.Text = "Save & Quit";
            this.btnSaveAndQuit.UseVisualStyleBackColor = true;
            this.btnSaveAndQuit.Click += new System.EventHandler(this.btnSaveAndQuit_Click);
            // 
            // btnViewCharts
            // 
            this.btnViewCharts.Location = new System.Drawing.Point(392, 14);
            this.btnViewCharts.Name = "btnViewCharts";
            this.btnViewCharts.Size = new System.Drawing.Size(102, 28);
            this.btnViewCharts.TabIndex = 2;
            this.btnViewCharts.Text = "Charts";
            this.btnViewCharts.UseVisualStyleBackColor = true;
            this.btnViewCharts.Click += new System.EventHandler(this.btnViewCharts_Click);
            // 
            // btnViewFormSettings
            // 
            this.btnViewFormSettings.Location = new System.Drawing.Point(677, 14);
            this.btnViewFormSettings.Name = "btnViewFormSettings";
            this.btnViewFormSettings.Size = new System.Drawing.Size(77, 28);
            this.btnViewFormSettings.TabIndex = 1;
            this.btnViewFormSettings.Text = "Settings";
            this.btnViewFormSettings.UseVisualStyleBackColor = true;
            // 
            // btnViewFormAddStock
            // 
            this.btnViewFormAddStock.Location = new System.Drawing.Point(277, 14);
            this.btnViewFormAddStock.Name = "btnViewFormAddStock";
            this.btnViewFormAddStock.Size = new System.Drawing.Size(109, 28);
            this.btnViewFormAddStock.TabIndex = 1;
            this.btnViewFormAddStock.Text = "Add Stock";
            this.btnViewFormAddStock.UseVisualStyleBackColor = true;
            this.btnViewFormAddStock.Click += new System.EventHandler(this.btnViewFormAddStock_Click);
            // 
            // btnViewFormManageSales
            // 
            this.btnViewFormManageSales.Location = new System.Drawing.Point(130, 14);
            this.btnViewFormManageSales.Name = "btnViewFormManageSales";
            this.btnViewFormManageSales.Size = new System.Drawing.Size(141, 28);
            this.btnViewFormManageSales.TabIndex = 1;
            this.btnViewFormManageSales.Text = "View / Manage Sales";
            this.btnViewFormManageSales.UseVisualStyleBackColor = true;
            this.btnViewFormManageSales.Click += new System.EventHandler(this.btnViewFormManageSales_Click);
            // 
            // btnViewFormAddSale
            // 
            this.btnViewFormAddSale.Enabled = false;
            this.btnViewFormAddSale.Location = new System.Drawing.Point(15, 14);
            this.btnViewFormAddSale.Name = "btnViewFormAddSale";
            this.btnViewFormAddSale.Size = new System.Drawing.Size(109, 28);
            this.btnViewFormAddSale.TabIndex = 0;
            this.btnViewFormAddSale.Text = "Add Sale";
            this.btnViewFormAddSale.UseVisualStyleBackColor = true;
            this.btnViewFormAddSale.Click += new System.EventHandler(this.btnViewFormAddSale_Click);
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(5, 79);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(227, 55);
            this.Title.TabIndex = 3;
            this.Title.Text = "Add Sale";
            // 
            // productIdLabel
            // 
            this.productIdLabel.AutoSize = true;
            this.productIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productIdLabel.Location = new System.Drawing.Point(70, 256);
            this.productIdLabel.Name = "productIdLabel";
            this.productIdLabel.Size = new System.Drawing.Size(112, 25);
            this.productIdLabel.TabIndex = 4;
            this.productIdLabel.Text = "Product ID";
            this.productIdLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.productIdLabel.Click += new System.EventHandler(this.productID_label_Click);
            // 
            // totalAmountLabel
            // 
            this.totalAmountLabel.AutoSize = true;
            this.totalAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalAmountLabel.Location = new System.Drawing.Point(424, 256);
            this.totalAmountLabel.Name = "totalAmountLabel";
            this.totalAmountLabel.Size = new System.Drawing.Size(163, 25);
            this.totalAmountLabel.TabIndex = 6;
            this.totalAmountLabel.Text = " Total Amount $";
            this.totalAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.totalAmountLabel.Click += new System.EventHandler(this.totalAmount_Click);
            // 
            // quantityLabel
            // 
            this.quantityLabel.AutoSize = true;
            this.quantityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantityLabel.Location = new System.Drawing.Point(91, 317);
            this.quantityLabel.Name = "quantityLabel";
            this.quantityLabel.Size = new System.Drawing.Size(92, 25);
            this.quantityLabel.TabIndex = 8;
            this.quantityLabel.Text = "Quantity";
            this.quantityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // quantity
            // 
            this.quantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity.Location = new System.Drawing.Point(188, 317);
            this.quantity.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.quantity.Name = "quantity";
            this.quantity.Size = new System.Drawing.Size(230, 31);
            this.quantity.TabIndex = 9;
            this.quantity.ValueChanged += new System.EventHandler(this.quantity_ValueChanged);
            // 
            // comments
            // 
            this.comments.Location = new System.Drawing.Point(592, 315);
            this.comments.Multiline = true;
            this.comments.Name = "comments";
            this.comments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.comments.Size = new System.Drawing.Size(230, 97);
            this.comments.TabIndex = 10;
            // 
            // commentsLabel
            // 
            this.commentsLabel.AutoSize = true;
            this.commentsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commentsLabel.Location = new System.Drawing.Point(472, 315);
            this.commentsLabel.Name = "commentsLabel";
            this.commentsLabel.Size = new System.Drawing.Size(114, 25);
            this.commentsLabel.TabIndex = 11;
            this.commentsLabel.Text = "Comments";
            this.commentsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateTime
            // 
            this.dateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTime.Location = new System.Drawing.Point(188, 381);
            this.dateTime.Name = "dateTime";
            this.dateTime.Size = new System.Drawing.Size(366, 31);
            this.dateTime.TabIndex = 12;
            this.dateTime.Value = new System.DateTime(2018, 9, 10, 2, 22, 13, 0);
            // 
            // dateTimeLabel
            // 
            this.dateTimeLabel.AutoSize = true;
            this.dateTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeLabel.Location = new System.Drawing.Point(66, 386);
            this.dateTimeLabel.Name = "dateTimeLabel";
            this.dateTimeLabel.Size = new System.Drawing.Size(116, 25);
            this.dateTimeLabel.TabIndex = 13;
            this.dateTimeLabel.Text = "Date/ Time";
            this.dateTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // confirmButton
            // 
            this.confirmButton.Location = new System.Drawing.Point(713, 489);
            this.confirmButton.Name = "confirmButton";
            this.confirmButton.Size = new System.Drawing.Size(109, 28);
            this.confirmButton.TabIndex = 3;
            this.confirmButton.Text = "Finish";
            this.confirmButton.UseVisualStyleBackColor = true;
            this.confirmButton.Click += new System.EventHandler(this.confirmButton_Click);
            // 
            // addItemButton
            // 
            this.addItemButton.Location = new System.Drawing.Point(713, 436);
            this.addItemButton.Name = "addItemButton";
            this.addItemButton.Size = new System.Drawing.Size(109, 28);
            this.addItemButton.TabIndex = 14;
            this.addItemButton.Text = "Add Product";
            this.addItemButton.UseVisualStyleBackColor = true;
            this.addItemButton.Click += new System.EventHandler(this.addItemButton_Click);
            // 
            // productID
            // 
            this.productID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productID.Location = new System.Drawing.Point(188, 256);
            this.productID.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.productID.Name = "productID";
            this.productID.Size = new System.Drawing.Size(230, 31);
            this.productID.TabIndex = 15;
            this.productID.ValueChanged += new System.EventHandler(this.productID_ValueChanged);
            // 
            // totalAmount
            // 
            this.totalAmount.DecimalPlaces = 2;
            this.totalAmount.Enabled = false;
            this.totalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalAmount.Location = new System.Drawing.Point(593, 256);
            this.totalAmount.Maximum = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this.totalAmount.Name = "totalAmount";
            this.totalAmount.Size = new System.Drawing.Size(230, 31);
            this.totalAmount.TabIndex = 16;
            // 
            // lblSelectedItem
            // 
            this.lblSelectedItem.AutoSize = true;
            this.lblSelectedItem.Location = new System.Drawing.Point(188, 237);
            this.lblSelectedItem.Name = "lblSelectedItem";
            this.lblSelectedItem.Size = new System.Drawing.Size(95, 13);
            this.lblSelectedItem.TabIndex = 17;
            this.lblSelectedItem.Text = "SELECTED_ITEM";
            // 
            // AddSale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 787);
            this.Controls.Add(this.lblSelectedItem);
            this.Controls.Add(this.totalAmount);
            this.Controls.Add(this.productID);
            this.Controls.Add(this.addItemButton);
            this.Controls.Add(this.confirmButton);
            this.Controls.Add(this.dateTimeLabel);
            this.Controls.Add(this.dateTime);
            this.Controls.Add(this.commentsLabel);
            this.Controls.Add(this.comments);
            this.Controls.Add(this.quantity);
            this.Controls.Add(this.quantityLabel);
            this.Controls.Add(this.totalAmountLabel);
            this.Controls.Add(this.productIdLabel);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.pnlMenu);
            this.Name = "AddSale";
            this.Text = "Add Sale";
            this.Load += new System.EventHandler(this.AddSale_Load);
            this.pnlMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.quantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalAmount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnSaveAndQuit;
        private System.Windows.Forms.Button btnViewCharts;
        private System.Windows.Forms.Button btnViewFormSettings;
        private System.Windows.Forms.Button btnViewFormAddStock;
        private System.Windows.Forms.Button btnViewFormManageSales;
        private System.Windows.Forms.Button btnViewFormAddSale;
		private System.Windows.Forms.Label Title;
		private System.Windows.Forms.Label productIdLabel;
		private System.Windows.Forms.Label totalAmountLabel;
		private System.Windows.Forms.Label quantityLabel;
		private System.Windows.Forms.NumericUpDown quantity;
		private System.Windows.Forms.TextBox comments;
		private System.Windows.Forms.Label commentsLabel;
		private System.Windows.Forms.DateTimePicker dateTime;
		private System.Windows.Forms.Label dateTimeLabel;
		private System.Windows.Forms.Button confirmButton;
		private System.Windows.Forms.Button addItemButton;
		private System.Windows.Forms.NumericUpDown productID;
		private System.Windows.Forms.NumericUpDown totalAmount;
        private System.Windows.Forms.Label lblSelectedItem;
    }
}

