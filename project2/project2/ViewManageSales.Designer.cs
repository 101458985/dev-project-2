﻿namespace project2
{
    partial class ViewManageSales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnCharts = new System.Windows.Forms.Button();
            this.btnSaveAndQuit = new System.Windows.Forms.Button();
            this.btnViewFormSettings = new System.Windows.Forms.Button();
            this.btnViewFormAddStock = new System.Windows.Forms.Button();
            this.btnViewFormManageSales = new System.Windows.Forms.Button();
            this.btnViewFormAddSale = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvSales = new System.Windows.Forms.DataGridView();
            this.pnlEdit = new System.Windows.Forms.Panel();
            this.numQuantity = new System.Windows.Forms.NumericUpDown();
            this.numTotalAmount = new System.Windows.Forms.NumericUpDown();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnSaveChanges = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvOrders = new System.Windows.Forms.DataGridView();
            this.MonthSelect = new System.Windows.Forms.NumericUpDown();
            this.MonthLabel = new System.Windows.Forms.Label();
            this.monthlyToggle = new System.Windows.Forms.CheckBox();
            this.btnGenerateReport = new System.Windows.Forms.Button();
            this.pnlMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSales)).BeginInit();
            this.pnlEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MonthSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnlMenu.Controls.Add(this.btnCharts);
            this.pnlMenu.Controls.Add(this.btnSaveAndQuit);
            this.pnlMenu.Controls.Add(this.btnViewFormSettings);
            this.pnlMenu.Controls.Add(this.btnViewFormAddStock);
            this.pnlMenu.Controls.Add(this.btnViewFormManageSales);
            this.pnlMenu.Controls.Add(this.btnViewFormAddSale);
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(939, 58);
            this.pnlMenu.TabIndex = 1;
            // 
            // btnCharts
            // 
            this.btnCharts.Location = new System.Drawing.Point(392, 14);
            this.btnCharts.Name = "btnCharts";
            this.btnCharts.Size = new System.Drawing.Size(109, 28);
            this.btnCharts.TabIndex = 2;
            this.btnCharts.Text = "Charts";
            this.btnCharts.UseVisualStyleBackColor = true;
            this.btnCharts.Click += new System.EventHandler(this.btnCharts_Click);
            // 
            // btnSaveAndQuit
            // 
            this.btnSaveAndQuit.Location = new System.Drawing.Point(760, 14);
            this.btnSaveAndQuit.Name = "btnSaveAndQuit";
            this.btnSaveAndQuit.Size = new System.Drawing.Size(163, 28);
            this.btnSaveAndQuit.TabIndex = 1;
            this.btnSaveAndQuit.Text = "Save & Quit";
            this.btnSaveAndQuit.UseVisualStyleBackColor = true;
            this.btnSaveAndQuit.Click += new System.EventHandler(this.btnSaveAndQuit_Click);
            // 
            // btnViewFormSettings
            // 
            this.btnViewFormSettings.Location = new System.Drawing.Point(677, 14);
            this.btnViewFormSettings.Name = "btnViewFormSettings";
            this.btnViewFormSettings.Size = new System.Drawing.Size(77, 28);
            this.btnViewFormSettings.TabIndex = 1;
            this.btnViewFormSettings.Text = "Settings";
            this.btnViewFormSettings.UseVisualStyleBackColor = true;
            // 
            // btnViewFormAddStock
            // 
            this.btnViewFormAddStock.Location = new System.Drawing.Point(277, 14);
            this.btnViewFormAddStock.Name = "btnViewFormAddStock";
            this.btnViewFormAddStock.Size = new System.Drawing.Size(109, 28);
            this.btnViewFormAddStock.TabIndex = 1;
            this.btnViewFormAddStock.Text = "Add Stock";
            this.btnViewFormAddStock.UseVisualStyleBackColor = true;
            this.btnViewFormAddStock.Click += new System.EventHandler(this.btnViewFormAddStock_Click);
            // 
            // btnViewFormManageSales
            // 
            this.btnViewFormManageSales.Location = new System.Drawing.Point(130, 14);
            this.btnViewFormManageSales.Name = "btnViewFormManageSales";
            this.btnViewFormManageSales.Size = new System.Drawing.Size(141, 28);
            this.btnViewFormManageSales.TabIndex = 1;
            this.btnViewFormManageSales.Text = "View / Manage Sales";
            this.btnViewFormManageSales.UseVisualStyleBackColor = true;
            this.btnViewFormManageSales.Click += new System.EventHandler(this.btnViewFormManageSales_Click);
            // 
            // btnViewFormAddSale
            // 
            this.btnViewFormAddSale.Location = new System.Drawing.Point(15, 14);
            this.btnViewFormAddSale.Name = "btnViewFormAddSale";
            this.btnViewFormAddSale.Size = new System.Drawing.Size(109, 28);
            this.btnViewFormAddSale.TabIndex = 0;
            this.btnViewFormAddSale.Text = "Add Sale";
            this.btnViewFormAddSale.UseVisualStyleBackColor = true;
            this.btnViewFormAddSale.Click += new System.EventHandler(this.btnViewFormAddSale_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(232, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Sales";
            // 
            // dgvSales
            // 
            this.dgvSales.AllowUserToAddRows = false;
            this.dgvSales.AllowUserToDeleteRows = false;
            this.dgvSales.AllowUserToResizeRows = false;
            this.dgvSales.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSales.Location = new System.Drawing.Point(225, 88);
            this.dgvSales.MultiSelect = false;
            this.dgvSales.Name = "dgvSales";
            this.dgvSales.ReadOnly = true;
            this.dgvSales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSales.Size = new System.Drawing.Size(698, 532);
            this.dgvSales.TabIndex = 4;
            this.dgvSales.SelectionChanged += new System.EventHandler(this.dgvSales_SelectionChanged);
            // 
            // pnlEdit
            // 
            this.pnlEdit.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnlEdit.Controls.Add(this.numQuantity);
            this.pnlEdit.Controls.Add(this.numTotalAmount);
            this.pnlEdit.Controls.Add(this.dtpDate);
            this.pnlEdit.Controls.Add(this.btnSaveChanges);
            this.pnlEdit.Controls.Add(this.label6);
            this.pnlEdit.Controls.Add(this.label5);
            this.pnlEdit.Controls.Add(this.label4);
            this.pnlEdit.Controls.Add(this.label3);
            this.pnlEdit.Controls.Add(this.txtComment);
            this.pnlEdit.Location = new System.Drawing.Point(225, 626);
            this.pnlEdit.Name = "pnlEdit";
            this.pnlEdit.Size = new System.Drawing.Size(698, 150);
            this.pnlEdit.TabIndex = 5;
            // 
            // numQuantity
            // 
            this.numQuantity.Location = new System.Drawing.Point(10, 68);
            this.numQuantity.Margin = new System.Windows.Forms.Padding(2);
            this.numQuantity.Name = "numQuantity";
            this.numQuantity.Size = new System.Drawing.Size(118, 20);
            this.numQuantity.TabIndex = 14;
            // 
            // numTotalAmount
            // 
            this.numTotalAmount.DecimalPlaces = 2;
            this.numTotalAmount.Location = new System.Drawing.Point(145, 67);
            this.numTotalAmount.Margin = new System.Windows.Forms.Padding(2);
            this.numTotalAmount.Name = "numTotalAmount";
            this.numTotalAmount.Size = new System.Drawing.Size(213, 20);
            this.numTotalAmount.TabIndex = 12;
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(145, 28);
            this.dtpDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(214, 20);
            this.dtpDate.TabIndex = 11;
            // 
            // btnSaveChanges
            // 
            this.btnSaveChanges.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveChanges.Location = new System.Drawing.Point(564, 104);
            this.btnSaveChanges.Name = "btnSaveChanges";
            this.btnSaveChanges.Size = new System.Drawing.Size(118, 23);
            this.btnSaveChanges.TabIndex = 10;
            this.btnSaveChanges.Text = "Save Changes";
            this.btnSaveChanges.UseVisualStyleBackColor = true;
            this.btnSaveChanges.Click += new System.EventHandler(this.btnSaveChanges_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(382, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Comments";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(142, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Total Amount $";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(142, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Date/Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Quantity";
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(385, 27);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(296, 61);
            this.txtComment.TabIndex = 4;
            this.txtComment.TextChanged += new System.EventHandler(this.txtComment_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(20, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 18);
            this.label7.TabIndex = 6;
            this.label7.Text = "Orders";
            // 
            // dgvOrders
            // 
            this.dgvOrders.AllowUserToAddRows = false;
            this.dgvOrders.AllowUserToDeleteRows = false;
            this.dgvOrders.AllowUserToResizeRows = false;
            this.dgvOrders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrders.Location = new System.Drawing.Point(16, 88);
            this.dgvOrders.MultiSelect = false;
            this.dgvOrders.Name = "dgvOrders";
            this.dgvOrders.ReadOnly = true;
            this.dgvOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOrders.Size = new System.Drawing.Size(203, 532);
            this.dgvOrders.TabIndex = 7;
            this.dgvOrders.SelectionChanged += new System.EventHandler(this.dgvOrders_SelectionChanged);
            // 
            // MonthSelect
            // 
            this.MonthSelect.Location = new System.Drawing.Point(23, 658);
            this.MonthSelect.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.MonthSelect.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MonthSelect.Name = "MonthSelect";
            this.MonthSelect.Size = new System.Drawing.Size(120, 20);
            this.MonthSelect.TabIndex = 9;
            this.MonthSelect.Tag = "";
            this.MonthSelect.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MonthSelect.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // MonthLabel
            // 
            this.MonthLabel.AutoSize = true;
            this.MonthLabel.Location = new System.Drawing.Point(21, 637);
            this.MonthLabel.Name = "MonthLabel";
            this.MonthLabel.Size = new System.Drawing.Size(37, 13);
            this.MonthLabel.TabIndex = 10;
            this.MonthLabel.Text = "Month";
            // 
            // monthlyToggle
            // 
            this.monthlyToggle.AutoSize = true;
            this.monthlyToggle.Location = new System.Drawing.Point(23, 684);
            this.monthlyToggle.Name = "monthlyToggle";
            this.monthlyToggle.Size = new System.Drawing.Size(118, 17);
            this.monthlyToggle.TabIndex = 11;
            this.monthlyToggle.Text = "View Monthly Sales";
            this.monthlyToggle.UseVisualStyleBackColor = true;
            this.monthlyToggle.CheckedChanged += new System.EventHandler(this.monthlyToggle_CheckedChanged_1);
            // 
            // btnGenerateReport
            // 
            this.btnGenerateReport.Location = new System.Drawing.Point(23, 708);
            this.btnGenerateReport.Name = "btnGenerateReport";
            this.btnGenerateReport.Size = new System.Drawing.Size(173, 23);
            this.btnGenerateReport.TabIndex = 12;
            this.btnGenerateReport.Text = "Generate Monthly Report CSV";
            this.btnGenerateReport.UseVisualStyleBackColor = true;
            this.btnGenerateReport.Visible = false;
            this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
            // 
            // ViewManageSales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 787);
            this.Controls.Add(this.btnGenerateReport);
            this.Controls.Add(this.monthlyToggle);
            this.Controls.Add(this.MonthLabel);
            this.Controls.Add(this.MonthSelect);
            this.Controls.Add(this.dgvOrders);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pnlEdit);
            this.Controls.Add(this.dgvSales);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlMenu);
            this.Name = "ViewManageSales";
            this.Text = "Form2";
            this.Activated += new System.EventHandler(this.ViewManageSales_Activated);
            this.pnlMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSales)).EndInit();
            this.pnlEdit.ResumeLayout(false);
            this.pnlEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MonthSelect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnSaveAndQuit;
        private System.Windows.Forms.Button btnViewFormSettings;
        private System.Windows.Forms.Button btnViewFormAddStock;
        private System.Windows.Forms.Button btnViewFormManageSales;
        private System.Windows.Forms.Button btnViewFormAddSale;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvSales;
        private System.Windows.Forms.Panel pnlEdit;
        private System.Windows.Forms.Button btnSaveChanges;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.NumericUpDown numQuantity;
        private System.Windows.Forms.NumericUpDown numTotalAmount;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgvOrders;
		private System.Windows.Forms.NumericUpDown MonthSelect;
		private System.Windows.Forms.Label MonthLabel;
        private System.Windows.Forms.CheckBox monthlyToggle;
        private System.Windows.Forms.Button btnCharts;
        private System.Windows.Forms.Button btnGenerateReport;
    }
}