﻿namespace project2
{
    partial class ManageStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.pnlMenu = new System.Windows.Forms.Panel();
			this.btnSaveAndQuit = new System.Windows.Forms.Button();
			this.btnViewFormManageStock = new System.Windows.Forms.Button();
			this.btnViewFormSettings = new System.Windows.Forms.Button();
			this.btnViewFormAddStock = new System.Windows.Forms.Button();
			this.btnViewFormManageSales = new System.Windows.Forms.Button();
			this.btnViewFormAddSale = new System.Windows.Forms.Button();
			this.pnlMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlMenu
			// 
			this.pnlMenu.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.pnlMenu.Controls.Add(this.btnSaveAndQuit);
			this.pnlMenu.Controls.Add(this.btnViewFormManageStock);
			this.pnlMenu.Controls.Add(this.btnViewFormSettings);
			this.pnlMenu.Controls.Add(this.btnViewFormAddStock);
			this.pnlMenu.Controls.Add(this.btnViewFormManageSales);
			this.pnlMenu.Controls.Add(this.btnViewFormAddSale);
			this.pnlMenu.Location = new System.Drawing.Point(0, 0);
			this.pnlMenu.Name = "pnlMenu";
			this.pnlMenu.Size = new System.Drawing.Size(939, 58);
			this.pnlMenu.TabIndex = 0;
			// 
			// btnSaveAndQuit
			// 
			this.btnSaveAndQuit.Location = new System.Drawing.Point(760, 14);
			this.btnSaveAndQuit.Name = "btnSaveAndQuit";
			this.btnSaveAndQuit.Size = new System.Drawing.Size(163, 28);
			this.btnSaveAndQuit.TabIndex = 1;
			this.btnSaveAndQuit.Text = "Save & Quit";
			this.btnSaveAndQuit.UseVisualStyleBackColor = true;
			// 
			// btnViewFormManageStock
			// 
			this.btnViewFormManageStock.Enabled = false;
			this.btnViewFormManageStock.Location = new System.Drawing.Point(392, 14);
			this.btnViewFormManageStock.Name = "btnViewFormManageStock";
			this.btnViewFormManageStock.Size = new System.Drawing.Size(102, 28);
			this.btnViewFormManageStock.TabIndex = 2;
			this.btnViewFormManageStock.Text = "Manage Stock";
			this.btnViewFormManageStock.UseVisualStyleBackColor = true;
			// 
			// btnViewFormSettings
			// 
			this.btnViewFormSettings.Location = new System.Drawing.Point(677, 14);
			this.btnViewFormSettings.Name = "btnViewFormSettings";
			this.btnViewFormSettings.Size = new System.Drawing.Size(77, 28);
			this.btnViewFormSettings.TabIndex = 1;
			this.btnViewFormSettings.Text = "Settings";
			this.btnViewFormSettings.UseVisualStyleBackColor = true;
			// 
			// btnViewFormAddStock
			// 
			this.btnViewFormAddStock.Location = new System.Drawing.Point(277, 14);
			this.btnViewFormAddStock.Name = "btnViewFormAddStock";
			this.btnViewFormAddStock.Size = new System.Drawing.Size(109, 28);
			this.btnViewFormAddStock.TabIndex = 1;
			this.btnViewFormAddStock.Text = "Add Stock";
			this.btnViewFormAddStock.UseVisualStyleBackColor = true;
			// 
			// btnViewFormManageSales
			// 
			this.btnViewFormManageSales.Location = new System.Drawing.Point(130, 14);
			this.btnViewFormManageSales.Name = "btnViewFormManageSales";
			this.btnViewFormManageSales.Size = new System.Drawing.Size(141, 28);
			this.btnViewFormManageSales.TabIndex = 1;
			this.btnViewFormManageSales.Text = "View / Manage Sales";
			this.btnViewFormManageSales.UseVisualStyleBackColor = true;
			// 
			// btnViewFormAddSale
			// 
			this.btnViewFormAddSale.Location = new System.Drawing.Point(15, 14);
			this.btnViewFormAddSale.Name = "btnViewFormAddSale";
			this.btnViewFormAddSale.Size = new System.Drawing.Size(109, 28);
			this.btnViewFormAddSale.TabIndex = 0;
			this.btnViewFormAddSale.Text = "Add Sale";
			this.btnViewFormAddSale.UseVisualStyleBackColor = true;
			// 
			// ManageStock
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(935, 787);
			this.Controls.Add(this.pnlMenu);
			this.Name = "ManageStock";
			this.Text = "Form4";
			this.Load += new System.EventHandler(this.ManageStock_Load);
			this.pnlMenu.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnViewFormManageStock;
        private System.Windows.Forms.Button btnViewFormSettings;
        private System.Windows.Forms.Button btnViewFormAddStock;
        private System.Windows.Forms.Button btnViewFormManageSales;
        private System.Windows.Forms.Button btnViewFormAddSale;
        private System.Windows.Forms.Button btnSaveAndQuit;
    }
}