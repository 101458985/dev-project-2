﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2
{
    public class Stock
    {
        public int qty;
        public double price;
        public string name;
        public int stockID;

        public Stock(string pname, int pID, int pQty, double pPrice)
        {
            name = pname;
            stockID = pID;
            qty = pQty;
            price = pPrice;
        }
    }
}
