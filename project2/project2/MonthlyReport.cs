﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2
{
    class MonthlyReport
    {
        List<SaleRecord> monthSales;
        Dictionary<int, int> itemSales;
        Dictionary<int, string> IDtoName;
        const string FILE_NAME = "month-report.txt";

        MonthlyReport(List<SaleRecord> aMonthsSales)
        {
            monthSales = aMonthsSales;
            PopulateDictionary();
            PrintToCSV();
        }

        private void PrintToCSV()
        {
            string output = "MONTHLY REPORT\n";
            // take all sales 
            // calculate total amount sold per item
            // print nameOfItem : valueSold over month
            Dictionary<int, int>.KeyCollection keys = itemSales.Keys;
            foreach(int key in keys)
            {

            }
        }

        private void PopulateDictionary()
        {
            foreach (SaleRecord s in monthSales)
            {
                int productID = s.ProductID;
                int quantity = s.Quantity;
                if (itemSales.ContainsKey(productID))
                {
                    int value = itemSales[productID];
                    itemSales[productID] = value + quantity;
                }
                else
                {
                    itemSales[productID] = quantity;
                }

            }
        }



    }
}
